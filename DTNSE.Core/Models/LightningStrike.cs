﻿using System;

namespace DTNSE.Core.Models
{
    public class LightningStrike
    {
        public int FlashType { get; set; }
        public long StrikeTime { get; set; }
        public double Latitude { get; set; }
        public double longitude { get; set; }
        public int PeakAmps { get; set; }
        public string Reserved { get; set; }
        public int ICHeight { get; set; }
        public long ReceivedTime { get; set; }
        public int NumberOfSensors { get; set; }
        public int Multiplicity { get; set; }

        private DateTime GetEpoch()
        {
            return new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        }

        public DateTime GetStrikeDate()
        { 
            return GetEpoch().AddSeconds(StrikeTime);
        }

        public DateTime GetReceivedDate()
        { 
            return GetEpoch().AddSeconds(ReceivedTime);
        }
    }
}
