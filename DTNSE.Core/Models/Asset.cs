﻿namespace DTNSE.Core.Models
{
    public class Asset
    {
        public string AssetName { get; set; }
        public string QuadKey { get; set; }
        public string AssetOwner { get; set; }
        public bool HasBeenAlerted { get; set; }
    }
}
