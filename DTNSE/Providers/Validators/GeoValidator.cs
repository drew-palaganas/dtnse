﻿namespace DTNSE.Providers.Validators
{
    public static class GeoValidator
    {
        public static bool ValidateLatitude(double value)
        {
            return value > -90 || value < 90;
        }

        public static bool ValidateLongitude(double value)
        {
            return value > -180 || value < 180;
        }
    }
}
