﻿using DTNSE.Data.Services;
using DTNSE.Helper;
using DTNSE.Providers;
using System;
using System.Linq;

namespace DTNSE
{
    class Program
    {
        static void Main(string[] args)
        {
            var assetDataService = new AssetDataService();
            var lightningStrikeDataService = new LightningStrikeDataService();

            var assetData = assetDataService.GetData();
            var lightningStikeData = lightningStrikeDataService.GetData();

            foreach(var data in lightningStikeData)
            {
                var quadKey = GeoHelper.LatLongToQuadKey(data.Latitude, data.longitude, GeoProvider.ZoomLevel);
                var match = assetData.SingleOrDefault(x => x.QuadKey == quadKey);

                if (match != null && match.HasBeenAlerted == false)
                {
                    match.HasBeenAlerted = true;
                    Console.WriteLine($"lightning alert for {match.AssetOwner}:{match.AssetName}");
                }
            }

            Console.ReadLine();
        }
    }
}
