﻿using DTNSE.Core.Models;
using DTNSE.Data.Providers;
using DTNSE.Data.Services.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;

namespace DTNSE.Data.Services
{
    public class LightningStrikeDataService : IDataService<LightningStrike>
    {
        public IEnumerable<LightningStrike> GetData()
        {
            var data = new List<LightningStrike>();

            try
            {
                var lines = File.ReadAllLines(PathProvider.LightningStrikeDataPath);

                foreach (var line in lines)
                {
                    var lightningStrikeData = JsonConvert.DeserializeObject<LightningStrike>(line);
                    data.Add(lightningStrikeData);
                }
            }
            catch(Exception ex)
            {

            }

            return data;
        }
    }
}
