﻿using DTNSE.Core.Models;
using DTNSE.Data.Providers;
using DTNSE.Data.Services.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;

namespace DTNSE.Data.Services
{
    public class AssetDataService : IDataService<Asset>
    {
        public IEnumerable<Asset> GetData()
        {
            var data = new List<Asset>();
            try
            {
                using (var reader = new StreamReader(PathProvider.AssetDataPath))
                {
                    var json = reader.ReadToEnd();
                    data = JsonConvert.DeserializeObject<List<Asset>>(json);
                }
            }
            catch (Exception ex) 
            {
                // log any errors here
            }

            return data;
        }
    }
}
