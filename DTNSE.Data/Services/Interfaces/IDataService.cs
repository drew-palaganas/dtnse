﻿using System.Collections.Generic;

namespace DTNSE.Data.Services.Interfaces
{
    public interface IDataService<T>
    {
        IEnumerable<T> GetData();
    }
}
