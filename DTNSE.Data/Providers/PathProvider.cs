﻿using System;
using System.IO;

namespace DTNSE.Data.Providers
{
    public class PathProvider
    {
        public static string ProjectPath
        {
            get
            {
                return Directory.GetParent(AppDomain.CurrentDomain.BaseDirectory).Parent.Parent.Parent.FullName;
            }
        }

        public static string DataFolderPath
        {
            get
            {
                return Path.Combine(ProjectPath, @"DTNSE.Data\Data");
            }
        }

        public static string AssetDataPath
        {
            get
            {
                return Path.Combine(DataFolderPath, @"assets.json");
            }
        }

        public static string LightningStrikeDataPath
        {
            get
            {
                return Path.Combine(DataFolderPath, @"lightning.json");
            }
        }
    }
}
