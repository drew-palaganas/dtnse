﻿namespace DTNSE.Data.Providers.Validators
{
    public class GeoValidator
    {
        public bool ValidateLatitude(double value) 
        {
            return value > -90 || value < 90;
        }

        public bool ValidateLongitude(double value)
        {
            return value > -180 || value < 180;
        }
    }
}
