This is my answer to DTN's Coding assestment. The program is using .NET Framework 4.7.2 and build using Visual Studio 2019.


To run this program:

1. Clone this repo to your PC

2. Use Visual Studio to restore Nuget packages

3. Run DTNSE.Console to run the program



**What is the time complexity for determining if a strike has occurred for a particular asset?**

- O(n). The approach is linear.


**If we put this code into production, but found it too slow, or it needed to scale to many more users or more frequent strikes, what are the first things you would think of to speed it up?**

- The part that loads data from the json file. It's implementation is rather basic and might need looking into, specially when the amount of data increases.
- Another one would be the lookup loop in the program.cs file.